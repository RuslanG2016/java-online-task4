public class CargoJet implements Jet {

    public CargoJet(int gasolineConsumtion, int payload, int range) {
        this.gasolineConsumtion = gasolineConsumtion;
        this.payload = payload;
        this.range = range;
    }

    public int getGasolineConsumtion() {
        return gasolineConsumtion;
    }

    public int getPayload() {
        return payload;
    }

    private int gasolineConsumtion;
    private int payload;
    private int range;


}
