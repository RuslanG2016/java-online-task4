public class PassengerJet implements Jet {
    private int passengers;
    private int gasolineConsumption;
    private int range;


    public PassengerJet(int passengers, int gasolineConsumption, int range) {
        this.passengers = passengers;
        this.gasolineConsumption = gasolineConsumption;
        this.range = range;
    }

    public int getPassengers() {
        return passengers;
    }

    public int getGasolineConsumption() {
        return gasolineConsumption;
    }





}
