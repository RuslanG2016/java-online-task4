import java.util.ArrayList;
import java.util.Collections;

public class Aircompany {
    public static void main(String[] args) {

        ArrayList <Jet> arrayList = new ArrayList<>();


            arrayList.add(new BigCargoJet(100, 100, 10000));
            arrayList.add(new BigCargoJet(80, 80, 10000));
            arrayList.add(new BigCargoJet(50, 75, 5000));

            arrayList.add(new SmallCargoJet(50, 50, 3000));            ;
            arrayList.add(new SmallCargoJet(50, 50, 3000));

            arrayList.add(new SmallCargoJet(30, 30, 2500));
            arrayList.add(new SmallCargoJet(30, 30, 2500));

            arrayList.add(new BigPassengerJet(50, 300, 10000));
            arrayList.add(new BigPassengerJet(40, 200, 5000));

            arrayList.add(new SmallPassengerJet(30, 100, 10000));
            arrayList.add(new SmallPassengerJet(10, 50, 5000));

        System.out.println(arrayList.size());


        int p = 0;
        int payload = 0;

        for (Jet o : arrayList) {
            if (o instanceof PassengerJet ){
              p += ((PassengerJet) o).getPassengers() ;
            }else if(o instanceof CargoJet){
                payload+= ((CargoJet) o).getPayload();
            }

        }
        System.out.println("Total capacity for passengers is  = " + p + "people");
        System.out.println("Total payload is  = " + payload + " tons");

        //functunality not yet fixed
        //Collections.sort( arrayList, new SortByPayLoad());

    }


}
