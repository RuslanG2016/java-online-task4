public interface Product {
    int getPrice ();
    void setPrice(int price);
    String getDescription();
    void setDescription(String description);

}



